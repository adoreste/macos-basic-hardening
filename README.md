# MacOS Basic Hardening

Use this script for **BASIC** hardening options on fresh macOS installs. Currently tested with macOS Catalina. It includes:

  - Enforce Password Policy
  - Enable Auto Updates
  - Disable metadata files on remote volumes
  - Enable Time Synchronization via NTP
  - Limit Ad Tracking
  - Set Firmware Password
  - Enable Firewall
  - Disable Captive Portal
  - Activate screenlock as soon as the screensaver starts
  - Show File Extentions
  - Disable save to iCloud by default
  - Disable Crash Reporter
  - Disable Bonjour Multicast Advertisements
  - Enable Filevault (Reboot needed)
  - Disable Automatic Mail Attachments Preview
  - Disable Time Machine prompts on new devices
  - Set default finder location to home folder and limits the search scope to the current folder
  - Disable Airdrop by default
  - Disable Safari auto-opening 'safe' files
  - Enable Gatekeeper Globally (Default on)
  - Disable IPv6
  - Disable Infrared Receiver
  - Restrict Sudoers File

These are just basic configuration changes. You should consider:
  - [PF](http://man.openbsd.org/pf.conf) is great, use it in order to manage the firewall. [Murus](https://www.murusfirewall.com) could help.
  - [Objective-See Products](https://objective-see.com/products.html) ARE GREAT.
  - [Google Santa](https://github.com/google/santa).
  - Stop using privileged accounts for daily tasks!!!!
  - Understanding and configuring the system according to your needs. 

References:

  - [NCSC](https://www.ncsc.gov.uk/collection/end-user-device-security?curPage=/collection/end-user-device-security/platform-specific-guidance) End User Device Security Platform Specific Guidance. 
  - https://github.com/drduh/macOS-Security-and-Privacy-Guide
  - https://github.com/ernw/hardening/blob/master/operating_system/osx/10.14/ERNW_Hardening_OS_X_Mojave.md
