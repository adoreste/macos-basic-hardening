#!/bin/zsh
# BASIC Hardening for macOS Mojave.
# LinkedIn: https://www.linkedin.com/in/andresdoreste
# References:
#  - https://www.ncsc.gov.uk/collection/end-user-device-security?curPage=/collection/end-user-device-security/platform-specific-guidance
#  - https://github.com/drduh/macOS-Security-and-Privacy-Guide
#  - https://github.com/ernw/hardening/blob/master/operating_system/osx/10.14/ERNW_Hardening_OS_X_Mojave.md

function airdrop_off {
    defaults write com.apple.NetworkBrowser DisableAirDrop -bool YES
}

function auto_updates {
    sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticCheckEnabled -bool true
    sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticDownload -bool true
    sudo defaults write /Library/Preferences/com.apple.commerce.plist AutoUpdateRestartRequired -bool true
    sudo defaults write /Library/Preferences/com.apple.commerce.plist AutoUpdate -bool true
    sudo defaults write /Library/Preferences/com.apple.SoftwareUpdate AutomaticallyInstallMacOSUpdates -int 1
    sudo defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1
}

function bonjour_multicast_off {
    sudo defaults write /Library/Preferences/com.apple.mDNSResponder.plist NoMulticastAdvertisements -bool YES
}

function captive_portal_off {
    sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.captive.control.plist Active -bool false
}

function crash_reporter_off {
    defaults write com.apple.CrashReporter DialogType none
}

function enforce_passwords {
    pwpolicy -u $(whoami) -setpolicy "minChars=8 requiresAlpha=1 requiresNumeric=1 maxMinutesUntilChangePassword=259200 usingHistory=5 usingExpirationDate=1 passwordCannotBeName=1 requiresMixedCase=1 requiresSymbol=1"
    pwpolicy getaccountpolicies -u $(whoami) | sed  "/Getting*/d" > password_policy.plist
    sudo pwpolicy setaccountpolicies password_policy.plist
    rm password_policy.plist
    sudo defaults write com.apple.loginwindow RetriesUntilHint -int 0
}

function extentions_on {
    defaults write NSGlobalDomain AppleShowAllExtensions -bool true
}

function filevault_on {
    sudo fdesetup enable
}

function finder_fix {
    defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"
    defaults write com.apple.finder NewWindowTarget -string "PfLo" && \
    defaults write com.apple.finder NewWindowTargetPath -string "file://${HOME}"
}

function firewall {
    sudo /usr/libexec/ApplicationFirewall/socketfilterfw --setglobalstate on
    sudo /usr/libexec/ApplicationFirewall/socketfilterfw --setloggingmode on
    sudo /usr/libexec/ApplicationFirewall/socketfilterfw --setstealthmode on
    sudo /usr/libexec/ApplicationFirewall/socketfilterfw --setallowsigned off
    sudo /usr/libexec/ApplicationFirewall/socketfilterfw --setallowsignedapp off
    sudo pkill -HUP socketfilterfw
}

function firmware_password {
    sudo firmwarepasswd -setpasswd
    sudo firmwarepasswd -verify
}

function gatekeeper_on {
    sudo spctl --master-enable
}

function icloud_save_off {
    defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false
}

function infrared_no {
    sudo defaults write com.apple.driver.AppleIRController DeviceEnabled -bool FALSE
}

function ipv6_no {
    sudo networksetup -setv6off Wi-Fi >/dev/null
    sudo networksetup -setv6off Ethernet >/dev/null
}

function limit_ad_tracking {
    defaults write /Users/"$(whoami)"/Library/Preferences/ByHost/com.apple.preference.security.privacy limitAdTrackingCached -int 0
    defaults write /Users/"$(whoami)"/Library/Preferences/com.apple.AdLib forceLimitAdTracking -int 1
    killall adprivacyd
    killall -SIGHUP cfprefsd
}

function metadata_files {
    sudo defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
    sudo defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true
}

function mail_attachments_off {
    defaults write com.apple.mail DisableInlineAttachmentViewing -bool yes
}

function remote_apple_events {
    sudo systemsetup -setremoteappleevents off
}

function restrict_sudoers {
    echo -e "Defaults timestamp_timeout=0\nDefaults tty_tickets" | sudo tee /private/etc/sudoers.d/01_restrictions
    sudo sed -ie '/.*HOME MAIL.*/s//# Defaults    env_keep += "HOME MAIL"/' /etc/sudoers
}

function safari_auto_opening_off {
    defaults write com.apple.Safari AutoOpenSafeDownloads -boolean NO
}

function screenlock {
    defaults write com.apple.screensaver askForPassword -int 1
    defaults write com.apple.screensaver askForPasswordDelay -int 0
    sudo pmset displaysleep 10
}

function time_machine_prompts_off {
    sudo defaults write /Library/Preferences/com.apple.TimeMachine DoNotOfferNewDisksForBackup -bool true
}

function time_sync {
    sudo systemsetup -setnetworktimeserver "time.euro.apple.com"
    sudo systemsetup -setusingnetworktime on 
    echo "restrict default ignore" | sudo tee -a /etc/ntp.conf
}

function main {
    # Enforce Password Policy
    echo -n "[?] Enforce Password Policy? (y/n): " && read q
    if [[ "$q" == "y" ]]; then enforce_passwords; fi
    # Enable Auto Updates
    echo -n "[?] Enable Auto Updates? (y/n): " && read q
    if [[ "$q" == "y" ]]; then auto_updates; fi
    # Disable .DS_Store on remote volumes or USB
    echo -n "[?] Disable metadata files on remote volumes? (y/n): " && read q
    if [[ "$q" == "y" ]]; then metadata_files; fi
    # Enable Time Synchronization via NTP
    echo -n "[?] Enable Time Synchronization via NTP? (y/n): " && read q
    if [[ "$q" == "y" ]]; then time_sync; fi
    # Limit Ad Tracking
    echo -n "[?] Limit Ad Tracking? (y/n): " && read q
    if [[ "$q" == "y" ]]; then limit_ad_tracking; fi
    # Set Firmware Password
    echo -n "[?] Set Firmware Password? (y/n): " && read q
    if [[ "$q" == "y" ]]; then firmware_password; fi
    # Enable Firewall
    echo -n "[?] Enable Firewall? (y/n): " && read q
    if [[ "$q" == "y" ]]; then firewall; fi
    # Disable Captive Portal
    echo -n "[?] Disable Captive Portal? (y/n): " && read q
    if [[ "$q" == "y" ]]; then captive_portal_off; fi
    # Activate screenlock as soon as the screensaver starts
    echo -n "[?] Activate screenlock as soon as the screensaver starts? (y/n): " && read q
    if [[ "$q" == "y" ]]; then screenlock; fi
    # Show Extentions
    echo -n "[?] Show File Extentions? (y/n): " && read q
    if [[ "$q" == "y" ]]; then extentions_on; fi
    # Save to iCloud
    echo -n "[?] Disable save to iCloud by default? (y/n): " && read q
    if [[ "$q" == "y" ]]; then icloud_save_off; fi
    # Disable crash reporter
    echo -n "[?] Disable Crash Reporter? (y/n): " && read q
    if [[ "$q" == "y" ]]; then crash_reporter_off; fi 
    # Disable Bonjour Multicast Advertisements
    echo -n "[?] Disable Bonjour Multicast Advertisements? (y/n): " && read q
    if [[ "$q" == "y" ]]; then bonjour_multicast_off; fi
    # Enable Filevault
    echo -n "[?] Enable Filevault (Reboot needed)? (y/n): " && read q
    if [[ "$q" == "y" ]]; then filevault_on; fi
    # Disable Automatic Mail Attachments Preview
    echo -n "[?] Disable Automatic Mail Attachments Preview? (y/n): " && read q
    if [[ "$q" == "y" ]]; then mail_attachments_off; fi 
    # Disable Time Machine prompts on new devices
    echo -n "[?] Disable Time Machine prompts on new devices? (y/n): " && read q
    if [[ "$q" == "y" ]]; then time_machine_prompts_off; fi 
    # Set default finder location to home folder and limits the search scope to the current folder
    echo -n "[?] Set default finder location to home folder and limits the search scope to the current folder? (y/n): " && read q
    if [[ "$q" == "y" ]]; then finder_fix; fi
    # Disable Airdrop
    echo -n "[?] Disable Airdrop by default? (y/n): " && read q
    if [[ "$q" == "y" ]]; then airdrop_off; fi
    # Disable Safari Auto-Opening
    echo -n "[?] Disable Safari auto-opening 'safe' files? (y/n): " && read q
    if [[ "$q" == "y" ]]; then safari_auto_opening_off; fi
    # Gatekeeper Globally
    echo -n "[?] Enable Gatekeeper Globally (Default on)? (y/n): " && read q
    if [[ "$q" == "y" ]]; then gatekeeper_on; fi
    # Disable IPv6
    echo -n "[?] Disable IPv6? (y/n): " && read q
    if [[ "$q" == "y" ]]; then ipv6_no; fi
    # Disable IPv6
    echo -n "[?] Disable Infrared Receiver? (y/n): " && read q
    if [[ "$q" == "y" ]]; then infrared_no; fi
    # Restrict Sudoers File
    echo -n "[?] Restrict Sudoers File? (y/n): " && read q
    if [[ "$q" == "y" ]]; then restrict_sudoers; fi
}
main